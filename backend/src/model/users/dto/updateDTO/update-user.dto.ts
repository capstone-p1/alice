import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDto } from '../createDTO/create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {}
