import { PartialType } from '@nestjs/mapped-types';
import { createRequestDTO } from '../createDTO/create-request.dto';

export class UpdateRequestDTO extends PartialType(createRequestDTO) {}
