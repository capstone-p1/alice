import { IsNotEmpty, IsNumber, IsString, MinLength } from "class-validator";
import { Image } from "src/model/components/entities/image.entity";

export class CreateUserDto {
    @IsString()
    firstName: string;

    @IsString()
    lastNameame: string;

    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    @MinLength(7)
    @IsString()
    password: string;

  
    age: number;

    phoneNumber: string;
}
