import { Module } from '@nestjs/common';
import { UsersService } from '../service/users.service';
import { UsersController } from '../controller/users.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../entities/user.entity';
import { CommentsModule } from 'src/model/user-components/module/comments.module';
import { LikesModule } from 'src/model/user-components/module/likes.module';
import { JourneyModule } from 'src/model/components/module/journey.module';
import { CaslModule } from 'src/casl/casl.module';
import { RequestsController } from '../controller/requests.controller';
import { RequestsService } from '../service/requests.service';



@Module({
  imports: [TypeOrmModule.forFeature([User]),
            LikesModule, CaslModule,
            CommentsModule, JourneyModule],
            
  controllers: [UsersController, RequestsController ],
  providers: [UsersService, RequestsService ],
  exports: [UsersService, RequestsService]
})
export class UsersModule {}
