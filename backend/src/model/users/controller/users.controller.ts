import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from '../service/users.service';
import { CreateUserDto } from '../dto/createDTO/create-user.dto';
import { UpdateUserDto } from '../dto/updateDTO/update-user.dto';
import jwtAuthenticationGuard from 'src/authentication/guard/jwtAuthentication.guard';
import { CreateAddressDTO } from '../../components/dto/create-address.dto';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory';
import { User } from '../entities/user.entity';
import { Action } from 'src/role/role.enum';


@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService,
    private caslAbilityFactory: CaslAbilityFactory) {}

  @Post()
  async registerUser(@Body() createUserDto: CreateUserDto) {
    return await this.usersService.registerUser(createUserDto);
  }

  @Get()
  async getAllUsers() {
    return await this.usersService.findAll();
  }

//  @Get(':username')
//  async getUserByUsername(@Param('username') username: string) {
//    return await this.usersService.findUserByUsername(username);
//  }

  @Get(':id')
  @UseGuards(jwtAuthenticationGuard)
  async getUserById(@Param('id') id: number){
    
      return await this.usersService.findUserById(id);
  }

  @Patch(':id')
  @UseGuards(jwtAuthenticationGuard)
  async editUserById(@Param('id') userId: number, @Body() updateUserDto: UpdateUserDto, @Request() req) {
    const ability = this.caslAbilityFactory.createForUser(req.user.userId)
    const user = await this.getUserById(userId)
    if(ability.can(Action.Update, user)){
        return [await this.usersService.editUserById(userId, updateUserDto), ability.can(Action.Update, user)];
    }
    throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
  }


  @Delete(':id')
  @UseGuards(jwtAuthenticationGuard)
  async remove(@Param('id') id: number, @Request() req) {
    const ability = this.caslAbilityFactory.createForUser(req.user.userId)
    const user = await this.getUserById(id)
    if(ability.can(Action.Update, user)){
        return await this.usersService.deleteUserById(id);
    }
    throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
  }
}
