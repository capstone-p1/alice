import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Req, Request, UseGuards } from "@nestjs/common";
import jwtAuthenticationGuard from "src/authentication/guard/jwtAuthentication.guard";
import { CaslAbilityFactory } from "src/casl/casl-ability.factory";
import { JourneyService } from "src/model/components/service/journey.service";
import { Action } from "src/role/role.enum";
import { createRequestDTO } from "../dto/createDTO/create-request.dto";
import { UpdateRequestDTO } from "../dto/updateDTO/update-request.dto";
import { RequestsService } from "../service/requests.service";
import { UsersService } from "../service/users.service";

@Controller("requests")
export class RequestsController {
    constructor(private readonly requestsService: RequestsService,
        private readonly journeysService: JourneyService,
        private readonly usersService: UsersService,
        private caslAbilityFactory: CaslAbilityFactory ) {}

    @Get()
    @UseGuards(jwtAuthenticationGuard)
    async getRequests() {
        return await this.requestsService.getRequests()
    }

    @Get(':id')
    @UseGuards(jwtAuthenticationGuard)
    async getOneReq(@Param('id') id: number){
        return await this.requestsService.getOneRequest(id)
    }

    @Get('/user/:userid')
    @UseGuards(jwtAuthenticationGuard)
    async getReqByUser(@Param('userid') userid: number){
        return await this.requestsService.getRequestsByOwnerId(userid)
    }

    @Post(':id')
    @UseGuards(jwtAuthenticationGuard)
    async sendRequest(@Param('id') id: number, @Request() req)
    {
        const journey = await this.journeysService.getJourneyByIdWithAuthor(id)
        const user = await req.user.userId
        console.log(req)
        if(journey && user) {
            return await this.requestsService.createRequest(journey, user)
        } else {
            throw new HttpException('Journey not found', HttpStatus.NOT_FOUND)
        }  
    }

    @Patch(':id')
    @UseGuards(jwtAuthenticationGuard)
    async updateReq(@Param('id') id: number, @Body() updateReqDTO: UpdateRequestDTO, @Request() req) {
        const ability = this.caslAbilityFactory.createForUser(req.user.userId)
        const request = await this.requestsService.getOneRequest(id)
        if(ability.can(Action.Update, request)) {
            return await this.requestsService.updateRequest(id, updateReqDTO)
        } else {
            throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED)
        }
    }

    @Delete(':id')
    @UseGuards(jwtAuthenticationGuard)
    async deleteReq(@Param('id') id:number, @Request() req) {
        const ability = this.caslAbilityFactory.createForUser(req.user.userId)
        const request = await this.requestsService.getOneRequest(id)
        if(ability.can(Action.Delete, request)) {
            return await this.requestsService.deleteReq(id)
        } else {
            throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED)
        }
    }
}