import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./user.entity";

@Entity('roles')
export class Role {
    @PrimaryGeneratedColumn()
    roleId: number;

    @Column()
    roleName: string;

    //@OneToMany(() => User, (user: User) => user.role)
    //public users: User[];

}