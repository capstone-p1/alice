import { Journey } from 'src/model/components/entities/journey.entity';
import { Genetic } from 'src/model/genetic/genetic.entity';
import {
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './user.entity';

@Entity('request')
export class Request extends Genetic {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(
    () => Journey,
    (journeyRequesting: Journey) => journeyRequesting.requests,
    { eager: true },
  )
  journeyRequesting: Journey;

  @Column({ nullable: true })
  ownerId: number;

  @ManyToOne(() => User, (sender: User) => sender.requests)
  sender: User;

  @Column({ nullable: true })
  senderId: number;

  @Column({ nullable: true })
  status: string;
}
