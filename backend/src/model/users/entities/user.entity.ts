
import { Comment } from "src/model/user-components/entities/comment.entity";
import { Journey } from "src/model/components/entities/journey.entity";
import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Image } from "src/model/components/entities/image.entity";
import { Like } from "src/model/user-components/entities/like.entity";
import { Genetic } from "src/model/genetic/genetic.entity";
import { Exclude } from "class-transformer";
import { Request } from "./request.entity";
//import { Role } from "./role.entity";


@Entity('users')
export class User extends Genetic {
    @PrimaryGeneratedColumn()
    userId: number;

    @Column({ nullable: true })
    firstName: string;

    @Column({ nullable: true })
    lastName: string;

    @Column({ unique: true })
    email: string;

    @Column({ nullable: true })
    @Exclude()
    password: string;

    @Column({ nullable: true })
    age: number;

    @Column({ nullable: true })
    phoneNumber: string;

    @Column({ nullable: true })
    isAdmin : boolean;

    // role
    //@ManyToOne(() => Role, (role: Role) => role.users, {
    //    eager: true
    //})
    //public role: Role;

    // 1 Image is useravatar
    @OneToOne(() => Image, {
        eager: true, //get address while fetch users 
        cascade: true //save address while saving user
    })
    @JoinColumn({name: 'avatarId'})
    public userAvatar: Image;

    // post comment and like on journey
    @OneToMany(() => Comment, (comment: Comment) => comment.author)
    public comments: Comment[];

    @OneToMany(() => Like, (like: Like) => like.author)
    public likes: Like[];

    // journey belong to user
    @OneToMany(() => Journey, (journey: Journey) => journey.author)
    public journeys: Journey[];
    
    @OneToMany(() => Request, (request: Request) => request.sender)
    public requests: Request[];

}

