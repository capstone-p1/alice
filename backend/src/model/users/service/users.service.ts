import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { CreateUserDto } from '../dto/createDTO/create-user.dto';
import { User } from '../entities/user.entity';
import { UpdateUserDto } from '../dto/updateDTO/update-user.dto';
import { resolve } from 'node:path';


@Injectable()
export class UsersService {
    private usersRepository: Repository<User>;
   
    constructor(private connection: Connection) {
        this.usersRepository = this.connection.getRepository(User);
    }

    //create a user
    async registerUser(createUserDto: CreateUserDto) {
        //create new instance
        const newUser = this.usersRepository.create(createUserDto);
        //save to db
        await this.usersRepository.save(newUser);
        return newUser;
    }

    //get all users
    async findAll() {
        return await this.usersRepository.find();
    }

    //get a user by username
    async findUserByUsername(email: string) {
        const user = await this.usersRepository.findOne({ email });
        try {
            if (user) {
                return user;
            }
        } catch (error) {
            throw new HttpException('username not found', HttpStatus.NOT_FOUND);
        }
    }

    //get a user by id
    async findUserById(userId: number) {
        const user = await this.usersRepository.findOne(userId, {relations: ['journeys', 'requests']})
        if (user) {
            return user;
        }
        throw new HttpException('user id not found', HttpStatus.NOT_FOUND);
    }

    //edit user by Id
    async editUserById(userId: number, updateUserDto: UpdateUserDto) {
        // throw error if not found
      
        const updateUser = await this.usersRepository.findOne(userId);
        if (updateUser) {
            return await this.usersRepository.update(userId, updateUserDto);
        }
        throw new HttpException('user id not found', HttpStatus.NOT_FOUND);

    }

    //delete user by id
    async deleteUserById(userId: number) {
        await this.usersRepository.delete(userId);
    }

}
