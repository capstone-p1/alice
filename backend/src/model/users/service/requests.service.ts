import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Journey } from "src/model/components/entities/journey.entity";
import { Connection, Repository } from "typeorm";
import { UpdateRequestDTO } from "../dto/updateDTO/update-request.dto";
import { Request } from "../entities/request.entity";
import { User } from "../entities/user.entity";

@Injectable()
export class RequestsService {
    private requestsRepository: Repository<Request>
    private usersRepository: Repository<User>
    private journeysRepository: Repository<Journey>
   
    constructor(private connection: Connection) {
        this.requestsRepository = this.connection.getRepository(Request)
        this.usersRepository = this.connection.getRepository(User)
        this.journeysRepository = this.connection.getRepository(Journey)
    }

    async createRequest(journey: Journey, user: User) {
        const newRequest = await this.requestsRepository.create({
            status: "pending",
            journeyRequesting: journey,
            ownerId: journey.authorId,
            sender: user,
            senderId: user.userId
        })
        await this.requestsRepository.save(newRequest)
        return newRequest
    }

    async getRequests() {
        return await this.requestsRepository.find({relations: ['sender']})
    }

    async getOneRequest(id: number) {
        const req = await this.requestsRepository.findOne(id, {relations: ['sender']})
        if(req) {
            return req
        } else {
            throw new HttpException('Request with Id not found', HttpStatus.NOT_FOUND)
        }
    }
    async updateRequest(id: number, updateReqDTO: UpdateRequestDTO) {
        const updateReq = await this.requestsRepository.update(id, updateReqDTO)
        if(updateReq) {
            const updatedReq = await this.requestsRepository.findOne(id)
            await this.addCompanion(updatedReq.status, updatedReq.senderId, updatedReq.journeyRequesting)
            return updatedReq
        } else {
            throw new HttpException('Request with Id not found', HttpStatus.NOT_FOUND)
        }
    }

    async deleteReq(id) {
        if(await this.requestsRepository.delete(id)){
            return 'Succesfully deleted'
        } else {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND)
        }
    }

    public async addCompanion(status: string, senderId: number, journey: Journey) {
        const companion = await this.usersRepository.findOne(senderId)
        console.log('companion', companion)
        if(status == 'approved') {
            journey.companions.push(companion)
            await this.journeysRepository.save(journey)
        } else if (status == 'declined') {
            console.log('companions', journey.companions)
            journey.companions.forEach(async person => {
                if(person.userId === companion.userId){
                    const index = journey.companions.indexOf(person)
                    journey.companions.splice(index)
                    await this.journeysRepository.save(journey)
                }
            });
            return 'The request was refused'
        } else {
            return new HttpException('Waiting for response', HttpStatus.PROCESSING)
        }
    }

    async getRequestsByOwnerId(userid: number){
        const results = await this.requestsRepository.createQueryBuilder()
                                    .select('request')
                                    .from(Request, 'request')
                                    .where('request.ownerId = :userid', {userid})
                                    .getMany()
        if(results) {
            return results
        } else {
            throw new HttpException('No requests received', HttpStatus.NOT_FOUND)
        }   
    }
}