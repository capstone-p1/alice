import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { JourneyModule } from "src/model/components/module/journey.module";
import { MilestoneController } from "../controller/milestone.controller";
import { placeController } from "../controller/place.controller";
import { Milestone } from "../entities/milestone.entity";
import { MilestonesService } from "../service/milestone.service";
import { PlacesService } from "../service/place.service";
import { PlaceModule } from "./places.module";

@Module({
    imports:[TypeOrmModule.forFeature([Milestone]), PlaceModule, JourneyModule],
    controllers: [ MilestoneController],
    providers: [ MilestonesService],
    exports: [MilestonesService]
})
export class MilestoneModule{}