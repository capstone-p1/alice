import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { placeController } from "../controller/place.controller";
import { Place } from "../entities/place.entity";
import { MilestonesService } from "../service/milestone.service";
import { PlacesService } from "../service/place.service";


@Module({
    imports: [TypeOrmModule.forFeature([Place])],
    controllers: [placeController],
    providers: [PlacesService, MilestonesService],
    exports: [PlacesService]

})
export class PlaceModule{}