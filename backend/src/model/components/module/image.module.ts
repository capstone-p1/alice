import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ImageController } from "../controller/image.controller";
import { Image } from "../entities/image.entity";
import { ImagesService } from "../service/image.service";

@Module({
    imports: [TypeOrmModule.forFeature([Image])],
    controllers: [ImageController],
    providers: [ImagesService],
    exports: [ImagesService]
})
export class ImageModule {}