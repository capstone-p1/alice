import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JourneyService } from '../service/journey.service';
import { JourneyController } from '../controller/journey.controller';
import { Journey } from '../entities/journey.entity';
import { CaslModule } from 'src/casl/casl.module';

@Module({
    imports: [TypeOrmModule.forFeature([Journey]), CaslModule],
  controllers: [JourneyController],
  providers: [JourneyService],
  exports: [JourneyService]
})
export class JourneyModule {}
