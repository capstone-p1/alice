import { Body, HttpCode, HttpException, HttpStatus, Injectable, Param } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { imageDto } from "../dto/create-image.dto";
import { createMilestoneDto } from "../dto/create-milestone.dto";
import { createPlaceDto } from "../dto/create-place.dto";
import { updatePlaceDto } from "../dto/update-place.dto";
import { Image } from "../entities/image.entity";
import { Milestone } from "../entities/milestone.entity";
import { Place } from "../entities/place.entity";

@Injectable()
export class PlacesService {
    private placeRepository: Repository<Place>;
    private milestoneRepository: Repository<Milestone>
    constructor( private connection: Connection){
        this.placeRepository = this.connection.getRepository(Place);
        this.milestoneRepository = this.connection.getRepository(Milestone)
    }



    async createPlace(placeDto: createPlaceDto, milestone: Milestone){
        const newPlace = await this.placeRepository.create(placeDto);
        milestone.places.push(newPlace);
        await this.milestoneRepository.save(milestone);
        await this.placeRepository.save(newPlace);
        return newPlace;
    }

    async getPlaces(){
        return await this.placeRepository.find({relations: ['images']});
    }


    async getPlaceById(placeId: number){
        const place = await this.placeRepository.findOne(placeId, {relations: ['images']});
        if(place){
            return place;
        }
        throw new HttpException('place Id not found', HttpStatus.NOT_FOUND);
        
    }

    async editPlaceById(placeId: number, updatePlaceDto: updatePlaceDto){
        if(this.getPlaceById(placeId)){
            const updatedPlace = await this.placeRepository.update(placeId, updatePlaceDto);
            return await this.getPlaceById(placeId);
        }
        throw new HttpException('place Id not found', HttpStatus.NOT_FOUND);
    }

    async deletePlaceById(placeId: number){
        if(await this.placeRepository.delete(placeId)){
            return 'delete place succesfully';
        }
        throw new HttpException('place Id not found', HttpStatus.NOT_FOUND);
    }
   
}