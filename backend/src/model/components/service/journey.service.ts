import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory';
import { CreateUserDto } from 'src/model/users/dto/createDTO/create-user.dto';
import { User } from 'src/model/users/entities/user.entity';
import { Connection, Repository } from 'typeorm';
import { imageDto } from '../dto/create-image.dto';
import { CreateJourneyDto } from '../dto/create-journey.dto';
import { UpdateJourneyDto } from '../dto/update-journey.dto';
import { Journey } from '../entities/journey.entity';

@Injectable()
export class JourneyService {
    private journeysRepository: Repository<Journey>;

    constructor(private connection: Connection) {
        this.journeysRepository = this.connection.getRepository(Journey);
    }

    async createJourney(createJourneyDto: CreateJourneyDto,user: User) {
            const newJourney = this.journeysRepository.create({
                ...createJourneyDto,
                author: user,
                authorId : user.userId,
                engDeparture : this.translate(createJourneyDto.departure),
                engDestination : this.translate(createJourneyDto.destination)

            });
            await this.journeysRepository.save(newJourney);
            return newJourney;
    }
    //get all journeys 
    async getJourneysWithAuthor() {
        return await this.journeysRepository.find({ relations: ['author','images', 'milestones', 'comments', 'likes' ] });
    }

    //get journey by Id
    async getJourneyByIdWithAuthor(id: number) {
        const journey = await this.journeysRepository.findOne(id, { relations: ['author','images', 'milestones', 'comments', 'likes', ] });
        if (journey) {
            journey.author.password = undefined;
            return journey;
        }
        throw new HttpException('journey with id not found', HttpStatus.NOT_FOUND);
    }

    //edit journey by Id
    async updateJourneyWithAuthor(id: number, journey: UpdateJourneyDto) {
        if(this.getJourneyByIdWithAuthor(id)){
            const updatedJourney = await this.journeysRepository.update(id,journey);
            return updatedJourney;
        } 
        
        throw new HttpException('journey with id not found', HttpStatus.NOT_FOUND);

    }

    //delete journey by id
    async deleteJourneyById(id: number){
        return await this.journeysRepository.delete(id);
    }
    
    //search by destination, departure
    async search(searchValue: string){
        const results = await this.journeysRepository.createQueryBuilder()
                                                    .select('journey')
                                                    .from(Journey, 'journey')
                                                    .where('LOWER(journey.departure) like LOWER(:searchValue)', {searchValue})
                                                    .orWhere('LOWER(journey.destination) like LOWER(:searchValue)', {searchValue})
                                                    .orWhere('LOWER(journey.engDestination) like LOWER(:searchValue)', {searchValue})
                                                    .orWhere('LOWER(journey.engDeparture) like LOWER(:searchValue)', {searchValue})
                                                    .getMany()
                                                    
        if(results) {
            return results
        } else {
            throw new HttpException('0 journey found', HttpStatus.NOT_FOUND);
        } 
    }
    // get by user Id
    async getJourneyByUser(userid: number){
        const results = await this.journeysRepository.createQueryBuilder()
                                                    .select('journey')
                                                    .from(Journey, 'journey')
                                                    .where('journey.authorUserId = :userid', {userid})
                                                    .getMany()                                 
        if(results) {
            return results
        } else {
            throw new HttpException('0 journey found', HttpStatus.NOT_FOUND);
        } 
    }
    //get by status
    async getJourneydByStatus(status: string){
        const results = await this.journeysRepository.createQueryBuilder()
                                                    .select('journey')
                                                    .from(Journey, 'journey')
                                                    .where('journey.status = :status', {status})
                                                    .getMany()                                 
        if(results) {
            return results
        } else {
            throw new HttpException('0 journey found', HttpStatus.NOT_FOUND);
        } 
    }
    //sorting
    async sortByDesc(type: string){ 
        const results = await this.journeysRepository.createQueryBuilder()
        .select('journey')
        .from(Journey, 'journey')
                                                    .orderBy(`journey.${type}`, 'DESC')
                                                    .getMany()
                                                                            
        if(results) {
            return results
        } else {
            throw new HttpException('0 journey found', HttpStatus.NOT_FOUND);
        }                                             
    }
    async sortByAsc(type: string){ 
        const results = await this.journeysRepository.createQueryBuilder()
        .select('journey')
        .from(Journey, 'journey')
                                                    .orderBy(`journey.${type}`, 'ASC')
                                                    .getMany()
                                                                            
        if(results) {
            return results
        } else {
            throw new HttpException('0 journey found', HttpStatus.NOT_FOUND);
        }                                             
    }

    public translate(vietnamese: string) {
        const vn = 'áàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶĐÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴ';
        const en = 'aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouuuuuuuuuuuyyyyyAAAAAAAAAAAAAAAAADEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYY';
        for (let i = 0; i < vn.length; i++) {
                if(vietnamese.includes(vn[i])) {
                vietnamese = vietnamese.replace(vn[i], en[i]);
            }
        }
        return vietnamese
    }
}

