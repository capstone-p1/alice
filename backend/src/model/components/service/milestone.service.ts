import { Body, HttpException, HttpStatus, Injectable, Request } from "@nestjs/common";
import { Journey } from "src/model/components/entities/journey.entity";
import { Connection, Repository } from "typeorm";
import { createMilestoneDto } from "../dto/create-milestone.dto";
import { updateMilestoneDto } from "../dto/update-milestone.dto";
import { Milestone } from "../entities/milestone.entity";
import { Place } from "../entities/place.entity";

@Injectable()
export class MilestonesService {
    private milestoneRepository: Repository<Milestone>
    
    constructor(private connection: Connection){
        this.milestoneRepository = this.connection.getRepository(Milestone);
        
    }

    //create milestone into journey
    async createMilestone(createMilestoneDto: createMilestoneDto, journey: Journey){
        const newMilestone = await this.milestoneRepository.create({...createMilestoneDto, journey})
        await this.milestoneRepository.save(newMilestone);
        return newMilestone;

    }

    //add place into milestone
    async addPlaceIntoMilestone(milestone: Milestone, places: Place[]){
        await this.milestoneRepository.save({...milestone,places})
    }

    //get all milestones
    async getMilestones(){
        return await this.milestoneRepository.find({relations: ['images','places']})
    }

    //get milestone by Id
    async getMilestoneById(id: number){
        const milestone = await this.milestoneRepository.findOne(id, {relations: ['images', 'places']});
        if(milestone){
            return milestone;
        }
        throw new HttpException('not found milestone Id', HttpStatus.NOT_FOUND);
    }

    //edit milesonte by Id
    async editMilestoneById(id: number, updateMilestoneDto: updateMilestoneDto){
        const updateMilestone = await this.milestoneRepository.findOne(id)
        if(updateMilestone){
            return await this.milestoneRepository.update(id,updateMilestoneDto);
        }
        throw new HttpException('not found milestone Id', HttpStatus.NOT_FOUND);
    }
    //delete milsetone by Id
    async deleteMilestoneById(id: number){
        if(await this.milestoneRepository.delete(id)){
            return "delete milesone successfully";
        }
        throw new HttpException('not found milestone Id', HttpStatus.NOT_FOUND);
    }
}