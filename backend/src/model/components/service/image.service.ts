import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { Image } from "../entities/image.entity";

@Injectable()
export class ImagesService {
    private imageRepository: Repository<Image>;
    constructor( private connection: Connection) {
        this.imageRepository = this.connection.getRepository(Image)
    }
    async getImages() {
        return await this.imageRepository.find()
    }

    async getOneImg(id: number) {
        const image = await this.imageRepository.findOne(id)
        if(image){
            return image
        } else {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND)
        }
    }

    async deletImg(id: number) {
        if(await this.imageRepository.delete(id)) {
            return "succesfully deleted"
        } else {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND)
        }
    }
}