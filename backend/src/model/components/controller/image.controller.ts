import { Controller, Delete, Get, Param } from "@nestjs/common";
import { ImagesService } from "../service/image.service";

@Controller('images')
export class ImageController {
    constructor(private readonly imagesService: ImagesService) {}

    @Get()
    async getAllImg() {
        return await this.imagesService.getImages()
    }
    
    @Get(':id')
    async getOneImg(@Param('id') id: number) {
        return await this.imagesService.getOneImg(id)
    }

    @Delete(':id')
    async deleteImg(@Param('id') id: number) {
        return await this.imagesService.deletImg(id)
    }
}