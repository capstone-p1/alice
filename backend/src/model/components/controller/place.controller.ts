import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Request, UseGuards } from "@nestjs/common";
import jwtAuthenticationGuard from "src/authentication/guard/jwtAuthentication.guard";
import { imageDto } from "../dto/create-image.dto";
import { createPlaceDto } from "../dto/create-place.dto";
import { updatePlaceDto } from "../dto/update-place.dto";
import { MilestonesService } from "../service/milestone.service";
import { PlacesService } from "../service/place.service";

@Controller('places')
export class placeController {
    constructor(
        private readonly placesService: PlacesService,
        private readonly milestonesService: MilestonesService
    ){
       
    }
    @UseGuards(jwtAuthenticationGuard)
    @Post(':id')
    async createPlace(@Body() placeDto: createPlaceDto, @Param('id') id: number ){   
        const milestone = await this.milestonesService.getMilestoneById(id);
        if(milestone) {
            return await this.placesService.createPlace(placeDto, milestone);
        }
        throw new HttpException('Milestone not found', HttpStatus.NOT_FOUND);
       
    }

    @Get()
    async getPlaces(){
        return await this.placesService.getPlaces();
    }

    @Get(':id')
    async getPlaceByName(@Param('id') id: number){
        return await this.placesService.getPlaceById(id);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Patch(':id')
    async editPlaceById(@Param('id') id: number, @Body() updateplaceDto: updatePlaceDto){
        return await this.placesService.editPlaceById(id, updateplaceDto);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Delete(':id')
    async deletePlaceById(@Param('id') id: number){
        return await this.placesService.deletePlaceById(id);
    }
    
}