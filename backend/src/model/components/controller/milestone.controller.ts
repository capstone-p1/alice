import { Body, Controller, Delete, Get, HttpException, HttpStatus, Param, Patch, Post, Request, UseGuards } from "@nestjs/common";
import jwtAuthenticationGuard from "src/authentication/guard/jwtAuthentication.guard";
import { JourneyService } from "src/model/components/service/journey.service";
import { imageDto } from "../dto/create-image.dto";
import { createMilestoneDto } from "../dto/create-milestone.dto";
import { createPlaceDto } from "../dto/create-place.dto";
import { updateMilestoneDto } from "../dto/update-milestone.dto";
import { Place } from "../entities/place.entity";
import { MilestonesService } from "../service/milestone.service";
import { PlacesService } from "../service/place.service";

@Controller('milestones')
export class MilestoneController {
    
    constructor(private readonly milestonesService: MilestonesService,
        private readonly journeysService: JourneyService
        ){}

   @Post(':journeyId')
   @UseGuards(jwtAuthenticationGuard)
   async createMilesonteIntoJourney(@Param('journeyId') journeyId: number, @Body() milestoneDto: createMilestoneDto){
       const journey = await this.journeysService.getJourneyByIdWithAuthor(journeyId);
       if(journey){
           return await this.milestonesService.createMilestone(milestoneDto, journey);
           
       }
   }
   @Get()
   async getMilestones(){
       return await this.milestonesService.getMilestones();
   }

   @Get(':id')
   async getMilestoneById(@Param('id') id:number){
       return await this.milestonesService.getMilestoneById(id);
   }

   @Patch(':id')
   @UseGuards(jwtAuthenticationGuard)
   async editMilestoneById(@Param('id') id:number, @Body() updateMilestoneDto : updateMilestoneDto){
       return await this.milestonesService.editMilestoneById(id,updateMilestoneDto);
   }

   @Delete(':id')
   @UseGuards(jwtAuthenticationGuard)
   async deleteMilestoneById(@Param('id') id : number){
       return await this.milestonesService.deleteMilestoneById(id);
   }
}