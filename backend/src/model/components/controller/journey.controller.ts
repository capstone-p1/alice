import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Req, Request, HttpException, HttpStatus } from '@nestjs/common';
import { JourneyService } from '../service/journey.service';
import { CreateJourneyDto } from '../dto/create-journey.dto';
import jwtAuthenticationGuard from 'src/authentication/guard/jwtAuthentication.guard';
import { UpdateJourneyDto } from '../dto/update-journey.dto';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory';
import { Action } from 'src/role/role.enum';


@Controller('journeys')
export class JourneyController {
    constructor(private readonly journeyService: JourneyService,
        private caslAbilityFactory: CaslAbilityFactory) { }

    @Post()
    @UseGuards(jwtAuthenticationGuard)
    async createJourney(@Body() createJourneyDto: CreateJourneyDto, @Request() req) {
        return await this.journeyService.createJourney(createJourneyDto, req.user.userId);
    }

    @Get()
    async getAllJourney() {
        return await this.journeyService.getJourneysWithAuthor();
    }

    @Get(`/sortbydesc=:type`)
    async sortJourneyByDesc(@Param('type') type: string) {
        return await this.journeyService.sortByDesc(type);
    }

    @Get(`/sortbyasc=:type`)
    async sortJourneyByAsc(@Param('type') type: string) {
        return await this.journeyService.sortByAsc(type);
    }

    @Get('/search=:searchValue')
    async searchByDeparture(@Param('searchValue') searchValue: string) {
        return await this.journeyService.search(searchValue)
    }

    @Get('/userid=:userid')
    @UseGuards(jwtAuthenticationGuard)
    async getJourneysByUser(@Param('userid') userid: number) {
        return await this.journeyService.getJourneyByUser(userid)
    }

    @Get('/status=:status')
    async getJourneysByStatus(@Param('status') status: string) {
        return await this.journeyService.getJourneydByStatus(status)
    }

    @Get(':id')
    async getJourneyById(@Param('id') id: number) {
        return await this.journeyService.getJourneyByIdWithAuthor(id);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Patch(':id')
    async updateJourneyById(@Param('id') id: number, @Body() updateJourneyDto: UpdateJourneyDto, @Request() req) {
        const ability = this.caslAbilityFactory.createForUser(req.user.userId)
        const journey = await this.journeyService.getJourneyByIdWithAuthor(id)
        if(ability.can(Action.Delete, journey)){
            return await this.journeyService.updateJourneyWithAuthor(id, updateJourneyDto);
        }
        throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
       
    }

  
    @Delete(':id')
    @UseGuards(jwtAuthenticationGuard)
    async deleteJourneyById(@Param('id') id: number, @Request() req) {
        const ability = this.caslAbilityFactory.createForUser(req.user.userId)
        const journey = await this.journeyService.getJourneyByIdWithAuthor(id)
        if(ability.can(Action.Delete, journey)){
            return await this.journeyService.deleteJourneyById(id);
        }
        throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
    }
}
