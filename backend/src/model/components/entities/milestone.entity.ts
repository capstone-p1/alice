import { Genetic } from "src/model/genetic/genetic.entity";
import { Journey } from "src/model/components/entities/journey.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique } from "typeorm";
import { Image } from "./image.entity";
import { Place } from "./place.entity";

@Entity('milestones')
export class Milestone extends Genetic{
    @PrimaryGeneratedColumn()
    public milestoneId: number;

    @Column({nullable: false, unique: false})
    public milestoneName: string;
    

    @ManyToOne(() => Journey, (journey: Journey) => journey.milestones, {
        onDelete: 'CASCADE'
    })
    public journey: Journey

    @OneToMany(() => Image, (image: Image) => image.milestone, {cascade: true, eager: true})
    public images: Image[];

    @ManyToMany(() => Place, {cascade: true, eager: true})
    @JoinTable()
    public places: Place[];

}