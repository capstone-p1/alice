
import { Genetic } from "src/model/genetic/genetic.entity";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Place } from "./place.entity";

@Entity('addresses')
export class Address extends Genetic {

    @PrimaryGeneratedColumn()
    addressId: number;

    @Column({nullable: true})
    addressLine: string;

    @Column({nullable: true})
    city: string;

    @Column({nullable: true})
    country: string;

    @Column({nullable: true})
    long: number;

    @Column({nullable: true})
    la: number;

    @OneToOne(() => Place, (place: Place) => place.address)
    public place: Place

}
