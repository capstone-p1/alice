import { Genetic } from "src/model/genetic/genetic.entity";
import { Address } from "src/model/components/entities/address.entity";
import { Column, Entity, Generated, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Image } from "./image.entity";
import { Milestone } from "./milestone.entity";

@Entity('places')
export class Place extends Genetic{
    @PrimaryGeneratedColumn()
    public placeId: number;

    @Column({nullable: false, unique: true})
    public placeName: string;

    @Column({nullable: true})
    public avgPrice: number;

    @OneToOne(() => Address, {
        eager: true, //get address while fetch users 
        cascade: true //save address while saving user
    })
    @JoinColumn({name: 'addressId'})
    public address: Address;


    @OneToMany(() => Image, (image: Image) => image.place, {cascade: true, eager: true})
    public images: Image[];

    
  


  
}