import { Genetic } from "src/model/genetic/genetic.entity";
import { User } from "src/model/users/entities/user.entity";
import { Column, Entity, JoinColumn, JoinTable, ManyToMany, ManyToOne, OneToOne, PrimaryColumn, PrimaryGeneratedColumn } from "typeorm";
import { Journey } from "./journey.entity";
import { Milestone } from "./milestone.entity";
import { Place } from "./place.entity";

@Entity('images')
export class Image extends Genetic{
    @PrimaryGeneratedColumn()
    imageId: number;
    
    @Column({unique: false})
    imageLink: string;

    // user avatar
    @OneToOne(() => User, (user: User) => user.userAvatar)
    public user: User;

    // images belong to journey
    @ManyToOne(() => Journey, (journey: Journey) => journey.images, {
        onDelete: 'CASCADE'
    })
    @JoinColumn({name: 'journeyId'})
    public journey: Journey;

    // images belong to milestone 
    @ManyToOne(() => Milestone, (milestone: Milestone) => milestone.images,{
        onDelete: 'CASCADE'
    })
    @JoinColumn({name: 'milestoneId'})
    public milestone: Milestone;

    // images belong to milestone
    @ManyToOne(() => Place, (place: Place) => place.images,{
        onDelete: 'CASCADE'
    }) 
    @JoinColumn({name: 'placeId'})
    public place: Place;
}