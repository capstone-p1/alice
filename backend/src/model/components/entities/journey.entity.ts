import { User } from "src/model/users/entities/user.entity";
import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Comment } from "../../user-components/entities/comment.entity";
import { Image } from "./image.entity";
import { Like } from "../../user-components/entities/like.entity";
import { Genetic } from "src/model/genetic/genetic.entity";
import { LikesModule } from "../../user-components/module/likes.module";
import { Milestone } from "src/model/components/entities/milestone.entity";
import { Exclude } from "class-transformer";
import { Request } from "src/model/users/entities/request.entity";


@Entity('journeys')
export class Journey extends Genetic{
    @PrimaryGeneratedColumn()
    journeyId: number;

    @Column({nullable: true})
    departure: string;
    @Column({nullable: true})
    destination: string;
    @Column({nullable: true})
    engDeparture: string;
    @Column({nullable: true})
    engDestination: string;
    @Column({nullable: true})
    description: string;
    @Column({nullable: true})
    startDate: Date;
    @Column({nullable: true})
    endDate: Date;
    @Column({nullable: true})
    status: string;
    @Column({nullable: true})
    //@Exclude()
    authorId: number;
   

    @ManyToOne(() => User, (author: User) => author.journeys)
    public author: User;

    @ManyToMany(() => User, {eager: true})
    @JoinTable()
    public companions: User[];

    @OneToMany(() => Milestone, (milestone: Milestone) => milestone.journey, {cascade: true, eager: true})
    @JoinTable({name: 'milestones'})
    public milestones: Milestone[]

    @OneToMany(() => Comment, (comment: Comment) => comment.journey)
    public comments: Comment[];

    @OneToMany(() =>Like, (like: Like) => like.journey)
    public likes: Like[];
    
    @OneToMany(() => Image,(image: Image) => image.journey, {cascade: true})
    public images: Image[];

    @OneToMany(() => Request, (request: Request) => request.journeyRequesting)
    public requests: Request[]
    
}
