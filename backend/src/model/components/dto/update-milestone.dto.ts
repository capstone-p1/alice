import { PartialType } from "@nestjs/mapped-types";
import { createMilestoneDto } from "./create-milestone.dto";

export class updateMilestoneDto extends PartialType(createMilestoneDto){}