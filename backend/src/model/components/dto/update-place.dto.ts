import { PartialType } from "@nestjs/mapped-types";
import { createPlaceDto } from "./create-place.dto";

export class updatePlaceDto extends PartialType(createPlaceDto){}