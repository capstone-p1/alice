export class CreateAddressDTO {
    addressLine: string;
    city: string;
    country: string;
    long: number;
    la: number
}