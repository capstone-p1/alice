export class CreateJourneyDto {
    departure: string;
    destination: string;
    description: string;
    startDate: Date;
    endDate: Date;
    status: string;

}
