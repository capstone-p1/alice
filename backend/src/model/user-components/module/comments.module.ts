import { Module } from '@nestjs/common';
import { CommentsController } from '../controller/comments.controller';
import { CommentsService } from '../service/comments.service';
import { JourneyService } from '../../components/service/journey.service';
import { JourneyModule } from 'src/model/components/module/journey.module';
import { CaslModule } from 'src/casl/casl.module';


@Module({
    imports: [JourneyModule, CaslModule],
  controllers: [CommentsController],
  providers: [CommentsService, JourneyService]
})
export class CommentsModule {}
