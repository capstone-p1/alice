import { Module } from '@nestjs/common';
import { LikesService } from '../service/likes.service';
import { LikesController } from '../controller/likes.controller';
import { JourneyModule } from '../../components/module/journey.module';
import { JourneyService } from '../../components/service/journey.service';
import { CaslModule } from 'src/casl/casl.module';

@Module({
    imports: [JourneyModule, CaslModule],
  controllers: [LikesController],
  providers: [LikesService, JourneyService]
})
export class LikesModule {}
