import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from 'src/model/users/entities/user.entity';
import { Connection, Repository } from 'typeorm';
import { CreateCommentDto } from '../dto/create-comment.dto';
import { UpdateCommentDto } from '../dto/update-comment.dto';
import { Comment } from '../entities/comment.entity';
import { Journey } from '../../components/entities/journey.entity';

@Injectable()
export class CommentsService {
    private cmtRepository: Repository<Comment>;

    constructor(private connection: Connection){
        this.cmtRepository = this.connection.getRepository(Comment);
    }
   

  async postComment(createCommentDto: CreateCommentDto, journey: Journey, author: User) {
    const newCmt = this.cmtRepository.create({...createCommentDto, journey, author});
    newCmt.authorId = author.userId;
    await this.cmtRepository.save(newCmt);

    return newCmt;
  }

  async getComments(){
      return await this.cmtRepository.find({relations: ['author', 'journey']})
  }

  async getCommentById(id: number){
      return await this.cmtRepository.findOne(id, {relations: ['author', 'journey']} )
  }


//  async getCommentsByI(id: number) {
//    const cmt = await this.cmtRepository.findOne();
//    if(cmt){
//        return cmt;
//    }
//    throw new HttpException('cmt not found', HttpStatus.NOT_FOUND);
//  }

  async update(id: number, updateCommentDto: UpdateCommentDto) {
    await this.cmtRepository.update(id, updateCommentDto);
    const updateCmt = await this.cmtRepository.findOne(id);
    if(updateCmt){
        return updateCmt;
    }
    throw new HttpException('cmt not found', HttpStatus.NOT_FOUND);
  }

  async remove(id: number) {
    await this.cmtRepository.delete(id);
  }
}
