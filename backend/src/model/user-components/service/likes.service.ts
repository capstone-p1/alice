import { HttpException, HttpService, HttpStatus, Injectable } from '@nestjs/common';
import { User } from 'src/model/users/entities/user.entity';
import { Connection, Repository } from 'typeorm';
import { Journey } from '../../components/entities/journey.entity';
import { Like } from '../entities/like.entity';

@Injectable()
export class LikesService {
    private likeRepository: Repository<Like>;
    constructor(private connection: Connection) {
        this.likeRepository = this.connection.getRepository(Like);
    }

  async createLike(user: User, journey: Journey) {
      const newLike = this.likeRepository.create({
        journey: journey,
          author: user});
          newLike.authorId = user.userId
      await this.likeRepository.save(newLike);
      return newLike;
  }

  async getLikes(){
      return await this.likeRepository.find({relations: ['author', 'journey']});
  }

  async getLikeById(id: number){
    const like = await this.likeRepository.findOne(id, { relations: ['author' ] });
    if (like) {
        return like;
    }
    throw new HttpException('like with id not found', HttpStatus.NOT_FOUND);
  }


  async unLike(id: number, userId: number) {
      try {
        //  const likeToDelete = await this.likeRepository.findOne(id);
        //  console.log(likeToDelete.author.userId);
        //  console.log(userId);

        //  if(likeToDelete.author.userId == userId){
           await this.likeRepository.delete(id);
           return "succesfully"
        //  } 
      } catch (error) {
          
          return new HttpException('You are not authorized to perform this action', HttpStatus.BAD_REQUEST);
      }
    
  }
}
