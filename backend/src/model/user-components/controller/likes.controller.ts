import { Controller, Get, Post, Body, Patch, Param, Delete, Request, UseGuards, HttpException, HttpStatus } from '@nestjs/common';
import { LikesService } from '../service/likes.service';
import { CreateLikeDto } from '../dto/create-like.dto';
import jwtAuthenticationGuard from 'src/authentication/guard/jwtAuthentication.guard';
import { CreateJourneyDto } from '../../components/dto/create-journey.dto';
import { Journey } from '../../components/entities/journey.entity';
import { JourneyService } from '../../components/service/journey.service';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory';
import { Action } from 'src/role/role.enum';


@Controller('likes')
export class LikesController {
    constructor(
        private readonly likesService: LikesService,
        private readonly journeysService: JourneyService,
        private caslAbilityFactory: CaslAbilityFactory
        ) { }

    @Post((':journeyid'))
    @UseGuards(jwtAuthenticationGuard)
    async createLike(@Request() req, @Param('journeyid') journeyId: number) {
        return await this.likesService.createLike(req.user.userId, await this.journeysService.getJourneyByIdWithAuthor(journeyId));
    }

    @Get()
    async getLikes(){
        return await this.likesService.getLikes();
    }
    
    @Get(':id')
    async getLikeById(@Param('id') id: number){
        return await this.likesService.getLikeById(id);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Delete(':id')
    async remove(@Param('id') id: number, @Request() req) {
        const ability = this.caslAbilityFactory.createForUser(req.user.userId)
        const like = await this.getLikeById(id)
        if(ability.can(Action.Delete, like)){
            return await this.likesService.unLike(id, req.user.userId);
        }
        throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
    }
}
