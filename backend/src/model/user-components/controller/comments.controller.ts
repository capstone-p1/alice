import { Controller, Get, Post, Body, Patch, Param, Delete, UseGuards, Request, HttpException, HttpStatus } from '@nestjs/common';
import jwtAuthenticationGuard from 'src/authentication/guard/jwtAuthentication.guard';
import { CreateCommentDto } from '../dto/create-comment.dto';
import { UpdateCommentDto } from '../dto/update-comment.dto';
import { Journey } from '../../components/entities/journey.entity';
import { CommentsService } from '../service/comments.service';
import { JourneyService } from '../../components/service/journey.service';
import { CaslAbilityFactory } from 'src/casl/casl-ability.factory';
import { Action } from 'src/role/role.enum';

@Controller('comments')
export class CommentsController {
  constructor(private readonly commentsService: CommentsService,
    private readonly journeysService: JourneyService,
    private caslAbilityFactory: CaslAbilityFactory)
   {}

  @Post((':journeyid'))
  @UseGuards(jwtAuthenticationGuard)
  async postComments(@Body() createCommentDto: CreateCommentDto, @Request()req, @Param('journeyid') journeyId: number) {
    return await this.commentsService.postComment(createCommentDto, await this.journeysService.getJourneyByIdWithAuthor(journeyId) ,req.user.userId);
  }

  @Get()
  async findAll() {
    return await this.commentsService.getComments();
  }

  @Get(':id')
  async getCommentById(@Param('id') id: number) {
    return this.commentsService.getCommentById(id);
  }
  @UseGuards(jwtAuthenticationGuard)
  @Patch(':id')
  async update(@Param('id') id: number, @Body() updateCommentDto: UpdateCommentDto, @Request() req) {
    const ability = this.caslAbilityFactory.createForUser(req.user.userId)
    const cmt = await this.commentsService.getCommentById(id)
    if(ability.can(Action.Delete, cmt)){
        return this.commentsService.update(id, updateCommentDto);
    }
    throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
  }
  @UseGuards(jwtAuthenticationGuard)
  @Delete(':id')
  async remove(@Param('id') id: number,@Request() req ) {
    const ability = this.caslAbilityFactory.createForUser(req.user.userId)
    const cmt = await this.commentsService.getCommentById(id)
    if(ability.can(Action.Delete, cmt)){
        return this.commentsService.remove(id);
    }
    throw new HttpException('Not authorization', HttpStatus.UNAUTHORIZED);
  }
}
