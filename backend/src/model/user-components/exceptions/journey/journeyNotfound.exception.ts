import { HttpException, HttpStatus, NotFoundException } from "@nestjs/common";

class JourneyNotfoundException extends NotFoundException {
    constructor(journeyId: number){
        super(`Journey with id ${journeyId} not found`);
    }
}