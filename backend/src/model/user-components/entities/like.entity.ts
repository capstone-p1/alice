import { Genetic } from "src/model/genetic/genetic.entity";
import { User } from "src/model/users/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Journey } from "../../components/entities/journey.entity";

@Entity('likes')
export class Like extends Genetic {
    @PrimaryGeneratedColumn()
    likeId: number;

    @Column({nullable: true})
    authorId: number

    @ManyToOne(() => User, (author: User) => author.likes)
    @JoinColumn({name: 'userId'})
    public author: User;

    @ManyToOne(() => Journey, (journey: Journey) => journey.likes, {
        onDelete: 'CASCADE'
    })
    @JoinColumn({name: 'journeyId'})
    public journey: Journey;
}
