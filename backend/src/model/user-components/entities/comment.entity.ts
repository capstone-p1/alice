import { Genetic } from "src/model/genetic/genetic.entity";
import { User } from "src/model/users/entities/user.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Journey } from "../../components/entities/journey.entity";

@Entity('comments')
export class Comment extends Genetic{
    @PrimaryGeneratedColumn()
    cmtId: number;

    @Column({nullable: false, unique: false})
    content: string;

    @Column({nullable: true})
    authorId: number

    @ManyToOne(() => User, (author: User) => author.comments)
    public author: User;

    @ManyToOne(() => Journey, (journey: Journey) => journey.comments, {
        onDelete: 'CASCADE'
    })
    public journey: Journey;
}
