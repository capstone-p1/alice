import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const ormconfig: TypeOrmModuleOptions = {
    type: 'postgres',
    username: 'postgres',
    password: 'redrose12',
    port: 5432,
    host: 'localhost',
    database: 'goweetest2',
    synchronize: true,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
};