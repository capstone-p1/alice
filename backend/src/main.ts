import { ClassSerializerInterceptor } from '@nestjs/common';
import { HttpAdapterHost, NestFactory, Reflector } from '@nestjs/core';
import * as cookieParser from 'cookie-parser';
import { AppModule } from './app.module';
import { validateInputPipe } from './pipes/validate.pipe';
import { ExceptionsLoggerFilter } from './utils/ExceptionsLogger.filter';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(cookieParser());
  //validation user input
  app.useGlobalPipes(new validateInputPipe());

  //use filter globally
  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new ExceptionsLoggerFilter(httpAdapter));

  //serialize
  app.useGlobalInterceptors(new ClassSerializerInterceptor(
      app.get(Reflector)
  ));

  await app.listen(3000);
}
bootstrap();
