import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ormconfig } from './typeOrm.config';
import { UsersModule } from './model/users/module/users.module';
import { CommentsModule } from './model/user-components/module/comments.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { MilestoneModule } from './model/components/module/milestone.module';
import { JourneyModule } from './model/components/module/journey.module';
import { LikesModule } from './model/user-components/module/likes.module';
import { CaslModule } from './casl/casl.module';
import { ImageModule } from './model/components/module/image.module';


@Module({
  imports: [
      UsersModule,
      TypeOrmModule.forRoot(ormconfig),
      JourneyModule,
      CommentsModule,
      LikesModule,
      MilestoneModule,
      AuthenticationModule,
      CaslModule,
      ImageModule
     
  ]
      
})
export class AppModule {}
