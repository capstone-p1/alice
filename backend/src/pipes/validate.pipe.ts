import { ArgumentMetadata, BadRequestException, Injectable, UnprocessableEntityException, ValidationPipe } from "@nestjs/common";
@Injectable()
export class validateInputPipe extends ValidationPipe{
    public async transfrom(value, metaData: ArgumentMetadata){
        try {
            return await super.transform(value, metaData);
        } catch (error) {
            if(error instanceof BadRequestException){
                throw new UnprocessableEntityException(this.handleError(error.message));
            }
        }
    }
    private handleError(errors){
        return errors.map(error => error.constraints)
    }
    
}