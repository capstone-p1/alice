import { Ability, AbilityBuilder, AbilityClass, ExtractSubjectType, InferSubjects } from "@casl/ability";
import { Injectable } from "@nestjs/common";
import { Journey } from "src/model/components/entities/journey.entity";
import { Comment } from "src/model/user-components/entities/comment.entity";
import { Like } from "src/model/user-components/entities/like.entity";
import { Request } from "src/model/users/entities/request.entity";
import { User } from "src/model/users/entities/user.entity";
import { Action } from "src/role/role.enum";

type Subjects = InferSubjects<typeof Journey | typeof Like | typeof Comment | typeof User | typeof Request> | 'all';
export type AppAbility = Ability<[Action, Subjects]>

@Injectable()
export class CaslAbilityFactory {
    createForUser(user: User) {
    
        const { can, cannot, build } = new AbilityBuilder<
        Ability<[Action, Subjects]>
        >(Ability as AbilityClass<AppAbility>);

        if(user.isAdmin == true) {
            can(Action.Manage, 'all')
        }
        else  {
            can(Action.Read, 'all');
            
        }
        can(Action.Create, 'all');
        can(Action.Update, User, { userId: user.userId });
        can(Action.Delete, User, {userId: user.userId});
        can(Action.Update, [Journey, Comment], {authorId: user.userId});
        can(Action.Delete, [Journey, Like, Comment], {authorId: user.userId});
        can(Action.Update, Request, { ownerId: user.userId })
        can(Action.Delete, Request, { senderId: user.userId })

        return build({
            detectSubjectType: item => item.constructor as ExtractSubjectType<Subjects>
        });
    }
}
