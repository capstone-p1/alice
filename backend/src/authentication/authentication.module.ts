import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from 'src/model/users/module/users.module';
import { UsersService } from 'src/model/users/service/users.service';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { jwtConstants } from './constants';
import { GoogleStrategy } from './strategy/google.strategy';
import { jwtStrategy } from './strategy/jwt.strategy';
import { localStrategy } from './strategy/local.strategy';

@Module({
    imports: [UsersModule, PassportModule,
                JwtModule.register({
                    secret: jwtConstants.JWT_TOKEN,
                    signOptions: { expiresIn: jwtConstants.JWT_EXPIRATION_TIME},
                })],
    providers: [AuthenticationService, localStrategy, jwtStrategy, GoogleStrategy],
    controllers: [AuthenticationController],
    exports: [AuthenticationService]
})
export class AuthenticationModule {}
