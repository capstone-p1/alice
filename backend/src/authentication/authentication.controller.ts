import { Body, Controller, Get, HttpCode, Post, Req, Request, Res, Response, UseGuards } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import requestWithUser from './interface/requestWithUser.interface';
import { LocalAuthenticationGuard } from './guard/localAuthentication.guard';
import jwtAuthenticationGuard from './guard/jwtAuthentication.guard';
import { UsersService } from 'src/model/users/service/users.service';
import { CreateUserDto } from 'src/model/users/dto/createDTO/create-user.dto';
import googleAuthenticationGuard from './guard/googleAuthentication.guard';



@Controller()
export class AuthenticationController {
    constructor(
        private readonly authenticationService: AuthenticationService,
        //private readonly usersService: UsersService
        ){}

    @Post('register')
    async register(@Body() registrationData: CreateUserDto){
        return this.authenticationService.register(registrationData);
    }

    @HttpCode(200)
    @UseGuards(LocalAuthenticationGuard)
    @Post('login')
    async login(@Request() req){
        return await this.authenticationService.login(req.user);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Post('logout')
    async logout(@Request() req, @Response() res){
        res.setHeader('Set-Cookie', this.authenticationService.getCookieForLogout());
        return res.sendStatus(200);
    }

    @UseGuards(jwtAuthenticationGuard)
    @Get()
    authenticate(@Request() req){
        const user = req.user;
        user.password = undefined;
        return user;
    }

    @Get('google')
    @UseGuards(googleAuthenticationGuard)
    async googleLogin(@Request() req) {}

    @Get('google/redirect')
    @UseGuards(googleAuthenticationGuard)
    googleAuthRedirect(@Req() req) {
      return this.authenticationService.googleLogin(req)
    }
   
}
