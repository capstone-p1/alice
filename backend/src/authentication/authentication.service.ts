import { UsersService } from "src/model/users/service/users.service";
import * as bcrypt from 'bcrypt';
import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

import { jwtConstants } from "./constants";
import { CreateUserDto } from "src/model/users/dto/createDTO/create-user.dto";

@Injectable()
export class AuthenticationService {
    constructor(
        private readonly usersService: UsersService,
        private readonly jwtService: JwtService,
    ){}

    public async register(registrationData: CreateUserDto){
        const hashedPassword = await bcrypt.hash(registrationData.password, 10);
        try {
            const createrUser = await this.usersService.registerUser({
                ...registrationData,
                password: hashedPassword
            });
            createrUser.password = undefined;
            const token = await this.getCookieWithJwtToken(createrUser.userId);
            return {createrUser, token};
        }
        catch(error){
            if (error?.code === errorCode.UniqueViolation) {
              throw new HttpException('Username already exists', HttpStatus.BAD_REQUEST);
            }
            throw new HttpException('Something went wrong', HttpStatus.INTERNAL_SERVER_ERROR);
          }
        
    }

    public async login(user){
        const token = await this.getCookieWithJwtToken(user);
        return {user, token};
    }

    public async checkAuthenticatedUser(email: string, password: string){
      try {
        const user = await this.usersService.findUserByUsername(email);
        await this.verifyPassword(password, user.password);
        user.password = undefined;
        return user;
       } catch (error) {
           throw new HttpException('Wrong password', HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }

    private async verifyPassword(password: string, hashedPassword: string){
        const checkPassword = await bcrypt.compare(password, hashedPassword);
        if(!checkPassword){
            throw new HttpException('Wrong password', HttpStatus.BAD_REQUEST);
        }
    }

    //jwt
    public async getCookieWithJwtToken(userId: number){
        const payload: tokenPayload = {userId};
        const token = await this.jwtService.signAsync(payload);
        return token;
        //return `Authentication=${token}; HttpOnly; Path=/; Max-Age=${jwtConstants.JWT_EXPIRATION_TIME}`;
    }

    

    public getCookieForLogout(){
        return [`Authentication=; HttpOnly; Path=/; Max-Age=0`];
    }

    public async googleLogin(req) {
        if(!req.user) {
            return 'No user from google'
        } else {
            try {
            const user = await this.usersService.registerUser(req.user)
            const token = req.user.accessToken
            return {user, token}
            } catch (error) {
                if (error) {
                    console.log(error)
                    const user = await this.usersService.findUserByUsername(req.user.email)
                    const token = req.user.accessToken
                    return {user, token}
                  }   
            }
        }
    }
   
}