import { Injectable, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt } from "passport-jwt";
import { Request } from "express";
import { UsersService } from "src/model/users/service/users.service";
import { jwtConstants } from "../constants";
@Injectable()
export class jwtStrategy extends PassportStrategy(Strategy, 'jwt'){
    constructor(
        private readonly usersService: UsersService ,
    ) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: jwtConstants.JWT_TOKEN,
            ignoreExpiration: false
        });
    }
    async validate(payload: any){
        const user = this.usersService.findUserByUsername(payload.userId);
        if(!user){
            throw new UnauthorizedException('Your not authorized');
        }
        return payload;
    }
    
 
}