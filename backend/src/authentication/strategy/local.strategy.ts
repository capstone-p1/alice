import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-local";
import { User } from "src/model/users/entities/user.entity";
import { AuthenticationService } from "../authentication.service";

@Injectable()
export class localStrategy extends PassportStrategy(Strategy, 'local'){
    constructor(private authenticationService: AuthenticationService){
        super({
            usernameField: "email"
        });
    }
    async validate(email: string, password: string): Promise<any> {
        return this.authenticationService.checkAuthenticatedUser(email,password);
    }
}