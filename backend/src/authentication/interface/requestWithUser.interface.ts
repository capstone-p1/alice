import { Request } from "express";
import { User } from "src/model/users/entities/user.entity";

interface requestWithUser extends Request{
    user: User;
}
export default requestWithUser;